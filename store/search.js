/**
 * State
 */

export const state = () => ({
  popular: [],
  suggestions: [],
})

/**
 * Getters
 */

export const getters = {
  randomPopular(state) {
    return state.popular[Math.floor(Math.random() * state.popular.length)]
  }
}

/**
 * Mutations types
 */

const UPDATE_POPULAR = 'UPDATE_POPULAR'
const UPDATE_SUGGESTIONS = 'UPDATE_SUGGESTIONS'

/**
 * Mutations
 */

export const mutations = {
  [UPDATE_POPULAR](state, newPopular) {
    state.popular = newPopular
  },
  [UPDATE_SUGGESTIONS](state, newSuggestions) {
    state.suggestions = newSuggestions
  }
}

/**
 * Actions
 */

export const actions = {
  async fetchPopular({ commit }) {
    const popularCountries = await this.$axios.$get('/places/countries/popular', {
      params: { preview: true },
    })
    commit(UPDATE_POPULAR, popularCountries)
  },
  async fetchSuggestions({ commit }, query) {
    if (query !== '') {
      const suggestions = await this.$axios.$get('/places/countries/suggest', {
        params: {
          query: query,
          search: true,
        }
      })
      if (suggestions && suggestions.results) {
        commit(UPDATE_SUGGESTIONS, suggestions.results)
      } else {
        commit(UPDATE_SUGGESTIONS, [])
      }
    }
  }
}
